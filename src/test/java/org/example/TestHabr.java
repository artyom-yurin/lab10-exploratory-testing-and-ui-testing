package org.example;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TestHabr {

    @Test
    public void testHabr() {

        WebDriver driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.habr.com/en/");
        System.out.println("Page Title is " + driver.getTitle());
        WebElement findBtn = driver.findElement(By.id("search-form-btn"));
        findBtn.click();
        WebElement findLine = driver.findElement(By.id("search-form-field"));
        findLine.sendKeys("SIEM Solutions Overview (Security Information and Event Management)");
        findLine.sendKeys(Keys.ENTER);

        driver.findElement(By.id("post_526302"));
        driver.quit();
    }
}
